## UC7: Confirmar registo de utilizador
## Formato breve
Um gestor de eventos solicita ao sistema o conjunto de utilizadores que efetuaram o registo e que ainda não foram confirmados no sistema. O sistema apresenta os dados solicitados ao gestor de eventos. O gestor de eventos seleciona os utilizadores pretendidos e submete-os ao sistema que, lhe solicita a confirma dessa sua seleção. O utilizador confirma os dados e o sistema regista os utilizadores selecionados como confirmados e informa o Gestor de eventos do sucesso da operação.
## SSD - Formato breve
![SSD_Confirma_Registo_Utilizador.jpg](https://bitbucket.org/repo/RXabA9/images/728009425-SSD_Confirma_Registo_Utilizador.jpg)